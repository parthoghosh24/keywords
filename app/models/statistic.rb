# == Schema Information
#
# Table name: statistics
#
#  id                               :integer          not null, primary key
#  number_of_top_adwords_results    :integer
#  number_of_bottom_adwords_results :integer
#  total_adwords_results            :integer
#  number_of_non_adwords_results    :integer
#  total_results                    :integer
#  total_results_text               :string
#  html                             :text
#  keyword_id                       :integer
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#

class Statistic < ApplicationRecord
  belongs_to :keyword
end
