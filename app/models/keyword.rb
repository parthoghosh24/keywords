# == Schema Information
#
# Table name: keywords
#
#  id         :integer          not null, primary key
#  slug       :string
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Keyword < ApplicationRecord
end
