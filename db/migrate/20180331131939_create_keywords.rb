class CreateKeywords < ActiveRecord::Migration[5.1]
  def change
    create_table :keywords do |t|
      t.string :slug
      t.string :token

      t.timestamps
    end
    add_index :keywords, :slug
    add_index :keywords, :token
  end
end
