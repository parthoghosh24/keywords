class CreateStatistics < ActiveRecord::Migration[5.1]
  def change
    create_table :statistics do |t|
      t.integer :number_of_top_adwords_results
      t.integer :number_of_bottom_adwords_results
      t.integer :total_adwords_results
      t.integer :number_of_non_adwords_results
      t.integer :total_results
      t.string :total_results_text
      t.text :html
      t.references :keyword, foreign_key: true

      t.timestamps
    end
    add_index :statistics, :number_of_top_adwords_results
    add_index :statistics, :number_of_bottom_adwords_results
    add_index :statistics, :total_adwords_results
    add_index :statistics, :number_of_non_adwords_results
    add_index :statistics, :total_results
  end
end
