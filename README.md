# README

A google search engine keywords analyzer
----------------------------------------

This app enables user to analyze their choice of keywords against google first SERP

Detailed System
----------------

* User can login and upload a list of keywords via CSV (Limit is till 1000 keywords).
* All those keywords are being searched on Google and the first search page is analyzed and reported against those keywords.
* Platform comes with an Oauth 2 RESTFUL JSON API for consumption via apps.
